const Ad = require("../models/Ad");

module.exports = function(app) {
  app.post("/ad", (req, res) => {
    var newAd = new Ad.m(req.body);
    newAd.save((err, s) => {
      console.log(req.body);
      res.send(req.body);
    });
  });

  app.get("/", (req, res) => {
    Ad.m.find({}, (err, docs) => {
      if (err) throw err;
      res.json(docs);
    });
  });

  app.get("/getSctruct", (req, res) => {
    SctructJSON = Ad.structJSON;

    mapInputTypes = {
      String: "text",
      Number: "Number",
      "[String]": "[String]"
    };

    res.json({
      struct: SctructJSON,
      map: mapInputTypes
    });
  });
};
