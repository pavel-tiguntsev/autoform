//Load config
const config = require("./config.json");

//Server
const express = require("express");
const app = express();
const port = config.appPort;
const bodyParser = require("body-parser");

//Database init
const murl = config.murl;
const dbName = config.db;
const mongoose = require("mongoose");
mongoose.connect(murl + "/" + dbName);

app.use(bodyParser.json());
app.use(function(req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type"
  );
  res.setHeader("Access-Control-Allow-Credentials", true);
  next();
});

var ads = require("./routes/ads.js");
ads(app);

app.listen(port, () => {
  console.log("Server running on ", port);
});
